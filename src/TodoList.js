import React from 'react';
import { TodoAdd } from './TodoAdd';
import TodoView from './TodoView';

export class TodoList extends React.PureComponent {

    state = {
        list:[] 
    };

    constructor(props){
        super(props);
        console.log('constructor called');
    }

    componentDidMount(){
        console.log('componentDidMount called');
    }

    componentDidUpdate(prevProps,prevState){
        console.log('componentDidUpdate called');
    }
    
    removeItemHandler = (id) => {
        const arrCopy = [...this.state.list];
        const filtered = arrCopy.filter(elem=>elem.id !== id);
        this.setState({list:filtered});
    }

    addItemHandler = (text) => {
        const arrCopy = [...this.state.list];
        arrCopy.push({id:Math.round(Math.random() * 1000),text});
        this.setState({list:arrCopy});
    }

    updateItemHandler = (id, text) => {
        const arrCopy = [...this.state.list];
        //arrCopy.push({id:Math.round(Math.random() * 1000),text});
        arrCopy.forEach(item => {
            if(item.id === id){
                item.text = text;
            }
        })
        this.setState({list:arrCopy});

    }

    render(){
        console.log('render called');
        const itemsView = this.state.list.map(elem => {
            return <div key={elem.id}>
                <TodoView id={elem.id} itemText={elem.text} onUpdateItem={this.updateItemHandler} onRemoveItem={()=>this.removeItemHandler(elem.id)}></TodoView>
                </div>
        })

        return <div className="component listItem">
            <h2>TodoList</h2>
            <div>{
                this.state.list.map(i => <span key={i.id}>{i.text}</span>)
                }</div>
            <TodoAdd onAddItem={this.addItemHandler}></TodoAdd>
            <div>
                {itemsView}
            </div>
        </div>
    }
}