import React, { useEffect, useState } from 'react';
import Toggle from './Toggle';

const Employee = (props) => {

    const [counter,setCounter] = useState(0)
    const [background,setBackground] = useState('red')

    useEffect(()=>{
        console.log('Inside use effect - both get changed')
    },[counter,background])

    useEffect(()=>{
        console.log('Inside use effect, with dependecy (wildcard)')
        return () => {
            console.log('On unmounting...');
        }
    },[])

    const buttonClickHandler = () => {
        console.log('Button clicked');
        setCounter(counter+1)
    }

    const changeBgHandler = () => {
        setBackground('Blue')
    }

    return <WithId id='ABC'>
            <div>
                Employee ({counter})
                <button onClick={buttonClickHandler}>Increment</button><br></br>
                {background}
                <button onClick={changeBgHandler}>Change Background</button>

                <br></br>
                {
                    counter % 2 === 0 ? <Toggle count={counter}></Toggle> : null
                }

            </div>
            <div>Testing</div>
        </WithId>
}


const WithId = (props) => <div id={props.id}>{props.children}</div>


export default Employee;

