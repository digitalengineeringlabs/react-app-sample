import React,{useState} from 'react'
import {subj} from './UserService'
import { Observable } from 'rxjs';

const NotifyComp = (props) => {
    const [msg,setMsg] = useState('');
    subj.subscribe((data)=>{
        setMsg(data)
    })
    return <div>Notify Message: {msg}</div>
}

export default NotifyComp;
// //
// const add = () => {
//     return new Observable((observer)=>{
//         //REST API call
//         observer.next(data);
//     })
// }

// //caller
// add.subscribe((data)=>{
    
// })