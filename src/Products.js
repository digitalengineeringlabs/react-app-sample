import React, { useState } from 'react'
import Product from './Product'

const Products = (props) => {

    const [products,setProducts] = useState([
            {id:1,title:'Television'},
            {id:2,title:'Radio'}
        ])

   const updateTitleHandler = (id,title) => {
        //update the state
        //sfsfdsfds
        console.log('ID : ',id);
        console.log('Title : ',title);
        const listCopy = [...products]
        listCopy.forEach(element => {
            if(element.id === id){
                element.title = title;
            }
        });
        setProducts(listCopy);
        //this.setState({products:listCopy});
    }

    
        return <div>
            <h4>Products</h4>
            {
                products.map(p => <Product key={p.id} prod={p} onEditTitle={updateTitleHandler}></Product>)
            }
            </div>
    
}

export default Products;