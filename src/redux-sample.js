const redux = require('redux')

const initialState = {
    counter: 0
}

const reducer = (state = initialState, action) => {
    //check what is to be done in action
    if(action.type === 'INC_COUNTER'){
        return {...state, counter:state.counter + 1}
    }
    //manipulate the state
    return state;
}

const store = redux.createStore(reducer);

store.subscribe(()=>{
    console.log('[subscriber 1]',store.getState());
})

store.subscribe(()=>{
    console.log('[subscriber 2]',store.getState());
})

//action will have, action type and payload
store.dispatch({type:'INC_COUNTER'});
store.dispatch({type:'INC_COUNTER'});
store.dispatch({type:'INC_COUNTER'});