import React, { useState } from 'react'
import {add} from './UserService'

const PublishComp = (props) => {

    const [name,setName] = useState('');
    return <div>
        <h3>Publisher</h3>
        <input type='text' value={name} onChange={(e)=>setName(e.target.value)}/>
        <button onClick={()=>add(name)}>Add</button>
    </div>
}

export default PublishComp;