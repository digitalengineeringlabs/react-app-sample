import React from 'react';
import { Route, Switch } from 'react-router';
import { NavLink,Redirect } from 'react-router-dom';
import Topics from './Topics';
import Sessions from './Sessions';
import Join from './Join';
import NewTopic from './NewTopic';
import Topic from './Topic';

export class Dashboard extends React.Component {
    render() {
        const menu = <ul>
            <li><NavLink to='/' exact>Home</NavLink></li>
            <li><NavLink to={{pathname:'/topics'}}>Topics</NavLink></li>
            <li><NavLink to={{pathname:'/sessions'}}>Sessions</NavLink></li>
            <li><NavLink to={{pathname:'/join'}}>Join</NavLink></li>
        </ul>
    //Styling (Active) Links
    //Passing query params and fragments in link
        //users?start=4
        //{pathname:'users',search:'?start=4'}
        //users#detail
        // {pathname:'',hash:'detail'}
    //Not found route ???

    //Inter-component state sharing using Behavioral Subject

    //Lifting State up
    //Redux
    
    //Formik

    //Unit Testing

        return <div>
            <h4>Dashboard</h4>
            {  menu          }
        
            <Route path='/' exact render={()=><h3>Home</h3>}></Route>
            <Route path="/topics" exact component={Topics}/>
            <Route path="/sessions" exact component={Sessions}/>
            <Route path="/join" exact component={Join}/>
            <Switch>
                <Route path='/topics/new' exact component={NewTopic}/>
                <Route path='/topics/:id' component={Topic}/>
            </Switch>
            {/* <Route path="/:junk" exact render={()=><h3>Page Not Found</h3>}/> */}
            {/* <Redirect from="/" to="/topics"></Redirect> */}
            
            </div>
    }
}