import React from 'react';
import './App.css';

//import { TodoList } from './TodoList';
import Users from './User';
import Products from './Products';
import Employee from './Employee';
import { RefComponent } from './RefComponent';
import { ErrorBoundary } from './ErrorBoundary';
import { Dashboard } from './Dashboard';
//import Student from './Student';
import { BrowserRouter } from 'react-router-dom';
import PublishComp from './PublishComp';
import AlertComp from './AlertComp';
import NotifyComp from './NotifyComp';
import CounterActions from './CounterActions';
import CounterResult from './CounterResult';

class App extends React.Component {

  // constructor(props){
  //   super(props);
  //   console.log('constructor called');
  // }

  // componentDidMount(){
  //     console.log('componentDidMount called');
  // }

  // componentDidUpdate(prevProps,prevState){
  //     console.log('componentDidUpdate called');
  // }

  render(){
    console.log('render called');
    //...
    //..
    return <div className="App component">
      {/* <h1>App</h1>
      <TodoList></TodoList> */}
      {/* <ErrorBoundary><Users></Users></ErrorBoundary> */}
      {/* <BrowserRouter>
        <Dashboard></Dashboard>
      </BrowserRouter> */}
      {/* <PublishComp></PublishComp>
      <AlertComp></AlertComp>
      <NotifyComp></NotifyComp> */}
      <CounterActions></CounterActions>
      <CounterResult></CounterResult>
      {/* <Products></Products> */}
      {/* <Employee></Employee> */}
      {/* <RefComponent></RefComponent> */}
      {/* <Student></Student> */}
    </div>
  }
}

export default App;
