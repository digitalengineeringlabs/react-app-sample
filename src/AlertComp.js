import React,{useState} from 'react'
import {subj} from './UserService'

const AlertComp = (props) => {
    const [msg,setMsg] = useState('');
    subj.subscribe((data)=>{
        setMsg(data)
    })
    return <div>Alert Message: {msg}</div>
}

export default AlertComp;