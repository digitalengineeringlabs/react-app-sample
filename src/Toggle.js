import React, { useEffect } from 'react'
import wrapClass from './hocs';

const Toggle = (props) => {

    useEffect(()=>{
        console.log('on Render');
        return ()=>{
            console.log('Do necessary cleanup before removing component from DOM');
        }
    })

    return <div>Toggle {props.count}</div>
}

export default wrapClass(Toggle, 'sample');
//export default Toggle;

//render
//componentDidMount
//componentDidUpdate
//componentWillUnMount