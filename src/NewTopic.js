import React, { useState } from 'react';
import { Link, Route, Redirect } from 'react-router-dom';

const NewTopic = (props) => {

    const [flag,setFlag] = useState(false)

    const submitHandler = () => {
        //props.history.push('/topics', {name: 'Arun'})
        setFlag(true);
    }

    return <div>
        
        <h2>New Topic</h2>
        <textarea></textarea>
        <button onClick={submitHandler}>Submit</button>
        {flag ? <Redirect to="/topics"></Redirect>: null }
    </div>
}

export default NewTopic;