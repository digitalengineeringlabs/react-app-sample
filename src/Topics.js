import React from 'react';
import { Link, Route } from 'react-router-dom';
import NewTopic from './NewTopic';

export default class Topics extends React.Component {
    state = {
        items: [
            {id: 1, title: 'Topic 1'},
            {id: 2, title: 'Topic 2'},
            {id: 3, title: 'Topic 3'}
        ]
    }
    render() {
        // someapp.com/topics/23?action=ADD&size=10&start=0
        console.log(this.props)
        return <div>
            <h1>Topics</h1>
            <Link to={{pathname:`${this.props.match.url}/new`}}>New Topic</Link>
            {
                this.state.items.map(item => <Link key={item.id} to={{pathname:`${this.props.match.url}/${item.id}`,search:'?size=10',hash:'detail'}}><h2>{item.title}</h2></Link>)
            }
        </div>;
    }

}
