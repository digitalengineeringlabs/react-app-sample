import React from 'react'
import Toggle from './Toggle'

const wrapClass = (WrappedComponent, className) => {
    return (props) => {
            return <div className={className}>
                <WrappedComponent {...props}></WrappedComponent>
            </div>
    }
}

export default wrapClass;