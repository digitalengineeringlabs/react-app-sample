import React from 'react';
import {connect} from 'react-redux';

class CounterResult extends React.Component {
    render() {
        return <div>Result : {this.props.ctr}
        <div>
            <button onClick={this.props.onSaveResult}>Save</button>
        </div>
        <div>
            {
                this.props.arr.map(item => <div>{item}</div>)
            }
        </div>
        </div>
    }
}

const mapStateToProps = (state) => {
    return {
        ctr: state.ctr.counter,
        arr: state.res.results
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onSaveResult: ()=>dispatch({type:"SAVE_RESULT"})
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(CounterResult);