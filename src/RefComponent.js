import React from 'react'

export class RefComponent extends React.Component {

    constructor(props){
        super(props)
        this.fullNameRef = React.createRef();
    }

    componentDidMount() {
        this.fullNameRef.current.focus()
    }

    render(){
        return <div>
            <span>Full Name</span>
            <input type='text' name='Full Name' ref={this.fullNameRef}/><br/>
            <span>Address</span>
            <input type='text' name='address'/>
        </div>
    }
}