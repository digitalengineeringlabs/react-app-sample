import React from 'react'
import axios from 'axios'

export default class Users extends React.Component{

    state = {
        users: [],
        toggle: true
    }

    componentDidMount() {

        // setTimeout(() => {
        //     throw new Error('Manual error')
        // }, 2000);

        axios.get('https://jsonplaceholder.typicode.com/users').then((result)=>{
            //this.setState({users:result.data});
            throw new Error()
        }).catch((err)=>{
                console.log('Error 1: ',err)
                throw err;
        });
        // fetch('https://jsonplaceholder.typicode.com/users')
        // .then(response => response.json())
        // .then(json => this.setState({users:json}))
    }

    toggleUserHandler = (id)=>{
        this.setState({toggle:!this.state.toggle})
    }

    render(){
        const view = this.state.users.map(user => <div key={user.id}>{user.name}
            <button>Delete</button>
            </div>)

        return <div>
            <h3>Users</h3>
            <button onClick={
                () => {
                    this.toggleUserHandler('1')
                }
            }>Toggle</button>
            { this.state.toggle ? view : null }
            </div>
    }
}