import {Subject} from 'rxjs'

//const subj = new Subject();

const subj = new Subject();

const add = (obj) => {
    //do the REST api call
    subj.next(obj)
}

export {subj,add};