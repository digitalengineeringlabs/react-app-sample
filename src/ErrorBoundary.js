import React from 'react'

export class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
      }
    componentDidCatch(error, info){
        console.log('Error in Boundary',error)
        console.log('Error Info',info);
        if(error) {            
            this.setState({hasError: true, errorMessage: error});
        }
    }

    render(){
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return <h1>Something went wrong.</h1>;
          }
        return this.props.children;
    }
}