import React from 'react'

class Product extends React.Component {
    state = {
        title: ''
    }

    componentDidMount() {
        this.setState({title:this.props.prod.title})
    }

    render(){
        return <div className='Product'>{this.props.prod.title}
        <input type="text" onChange={(e)=>this.setState({title:e.target.value})} value={this.state.title}></input>
        <button onClick={()=>this.props.onEditTitle(this.props.prod.id,this.state.title)}>Update</button>
        </div>
    }
}

export default Product;