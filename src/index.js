import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import './index.css';
import App from './App';
import { createStore, compose, combineReducers, applyMiddleware } from 'redux';
import counterReducer from './counterReducer';
import resultReducer from './resultReducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const logger = store => {
    return next => {
        return action => {
            console.log('[Middleware]:',action);
            next(action);
        }
    }
}

const store = createStore(
    combineReducers({ctr:counterReducer,res:resultReducer}), 
    composeEnhancers(applyMiddleware(logger)));
    
ReactDOM.render(<Provider store={store}><App/></Provider>, document.getElementById('root'));
// JSX - Syntactic Sugar of HTML