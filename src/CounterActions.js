import React from 'react';
import { connect } from 'react-redux';

class CounterActions extends React.Component {

    render() {
        return <div>
        <button onClick={this.props.onIncCounter}>Increment</button>
        <button onClick={this.props.onDecCounter}>Decrement</button>
        <button onClick={this.props.onAddCounter}>Add</button>
        <button onClick={this.props.onSubCounter}>Subtract</button>
        </div>
    }
}
//dispatch action , INC_COUNTER, DEC_COUNTER
const mapDispatchToProps = (dispatch) => {
    return {
        onIncCounter: ()=>dispatch(increment()), //used action creator
        onDecCounter: ()=>dispatch({type:'DEC_COUNTER'}),
        onAddCounter: ()=>dispatch({type:'ADD_COUNTER',val:5}),
        onSubCounter: ()=>dispatch({type:'SUB_COUNTER',val:5})
    }
}

//ACTION Creator
const increment = () => {
    return {type:'INC_COUNTER'};
}

export default connect(null,mapDispatchToProps)(CounterActions);

