//React Hooks - REact 16.8

//Class Component (Stateful component)
//State
//Life cycle

//Function Component (stateless component)
//Hooks - Javascript function - Built in, Custom Hooks

//useState
//useEffect
//useContext

import React, { useState } from 'react';

const Student = (props) => {

    // state = {
    //     student : {
    //         name: '',
    //         age: '',
    //         standard: ''
    //     }
    // }

    
    // int age = 0;

    const [student,setStudent] = useState([{name:'Ram',age:23,qualification:'Bachelors'}]);
    // const user = {name:'Arun',age:30, zip: 232323}
    // const abc = {...user, age:40,zip:999 }
    // console.log(abc)

    return <div>
        <h2>Student</h2> 
        <p>Name: {student.name}</p>
        <p>Age: {student.age}</p>
        <p>Qualification: {student.qualification}</p>
        <button onClick={ () => setStudent({...student, age:40}) }>Increment</button>
    </div>;
}

export default Student;
